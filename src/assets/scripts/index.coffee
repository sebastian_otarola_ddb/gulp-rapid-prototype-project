# Core Libraries
window.$ 		= require 'jquery/dist/jquery.min.js'
window._ 		= require 'underscore/underscore-min'
window.Backbone = require 'backbone/backbone-min'
Backbone.$ = $

# Setting up Router managers
RouterManager = require 'core/router-manager'

# Adding test model to see compilaton
View = require '../scripts/views/TodoView'

module.exports = class Application extends Backbone.View
	# Enabled Pushstate for Backbone
	Backbone.history.start({pushState: true})

	el: 'body'
	#Instanciating test model
	myView: new View()

# Running the application
window.App = new Application() || {}

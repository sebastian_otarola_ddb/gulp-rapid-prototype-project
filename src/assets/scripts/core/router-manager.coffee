#Added dynamic path buidling for routes with navigate function
# Get Instance is a Singleton that returns the Router.

class Router extends Backbone.Router
	routes:
		''		: 'index'
		'*path' : 'default'

module.exports = class RouterManager
	instance = null

	@getInstance: ->
		instance ?= new Router()

	@navigate: (route) =>
		this.getInstance().navigate(route.toString(), trigger: true)

	@default: =>
		console.log 'This is the routes navigation function. Execution goes here'

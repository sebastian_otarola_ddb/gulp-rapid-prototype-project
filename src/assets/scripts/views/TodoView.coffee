Template = require './TodoView.tpl'
Model = require '../models/TodoModel'

module.exports = class View extends Backbone.View

	el: 'main'
	template: _.template(Template)

	model: new Model()

	initialize: =>
		console.log @model
		@render()

	render: =>
		@$el.html( @template( @model ) )

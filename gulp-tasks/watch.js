gulp.task('watch', function(){
	browserSync.init({
		server: {
			baseDir: './dist'
		}
		/* Comment out Server basedir and run from a proxy at
		proxy: 'dev.test.environment'
		*/
	});

	gulp.watch([path.src + '/jade/**/*.jade'], ['jade']);
	gulp.watch([path.src + '/**/*.tmpl'], ['jade', 'scripts']).on('change', browserSync.reload);

	gulp.watch([path.src + '/assets/scripts/**/*.coffee'], ['scripts']);

	gulp.watch([path.src + '/assets/styles/**/*.styl'], ['styles']).on('change', browserSync.reload);

	gulp.watch([path.src + '/assets/images/**/*'], ['images']).on('change', browserSync.reload);


});

var browserify 		= require('browserify'),
	buffer 			= require('vinyl-buffer'),
	source 			= require('vinyl-source-stream'),
	uglify 			= require('gulp-uglify'),
	coffeeify		= require('coffeeify'),
	stringify		= require('stringify'),
	watchify		= require('watchify'),
	gutil			= require('gulp-util'),
	assign			= require('lodash.assign');

	var customOpts = {
		entries: [path.src + '/assets/scripts/index.coffee'],
		extensions: ['.coffee'],
		paths: [path.src + '/assets/scripts/'],
		debug: !isProduction
	};

	var opts = assign({}, watchify.args, customOpts);
	// var bundler = browserify(browserifyOpts);
	var	b = watchify(browserify(opts));

	b.transform(coffeeify);
	b.transform(stringify);

	b.on('update', bundle);
	b.on('log', gutil.log);

function bundle() {
	return b.bundle()
		.on('error', onError)
		.pipe(source('index.js'))
		.pipe(buffer())
		.pipe(gulpif(isProduction, uglify()))
		.pipe(gulp.dest(path.dest + '/assets/js'))
		.pipe(browserSync.stream());
		/* Above Line is Live Reload for coffeescript stream */
}

gulp.task('scripts', function() {
	gulp.src([path.src + '/assets/scripts/vendor/*.*'])
		.pipe(gulp.dest(path.dest + '/assets/js/vendor'));
		return bundle();
});

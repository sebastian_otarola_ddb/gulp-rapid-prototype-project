var imagemin = require('gulp-imagemin');
/* Insert gulp-cache here for proxy if needed */

gulp.task('images', function() {
	return gulp.src(path.src = '/assets/images/**/*')
	.pipe(imagemin({
		optimizationLevel: 3,
		progressive: true,
		interlaced: true
	}))
	.pipe(gulp.dest(path.dest + '/assets/images'));
});
